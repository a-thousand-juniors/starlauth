# pylint: disable=redefined-outer-name

from datetime import datetime

import httpx
import pytest
from sqlalchemy import create_engine
from sqlalchemy.exc import ProgrammingError
from sqlalchemy_utils import create_database, drop_database  # noqa: E0401
from starlette.config import environ

# configure environment variables for testing before importing
# application settings (or any other module that somehow reads from it)
# so it gets reflected there
environ["TESTING"] = "True"
environ["SECRET"] = "TESTING"

from starlauth import settings  # noqa: E402
from starlauth.app import app  # noqa: E402
from starlauth.resources import db  # noqa: E402
from starlauth.tables import metadata  # noqa: E402


@pytest.fixture(scope="session", autouse=True)
def create_test_database():
    """
    Create a clean test database every time the tests are run.
    """
    url = str(settings.DATABASE_URL)
    engine = create_engine(url)
    try:
        drop_database(url)
    except ProgrammingError:
        pass
    create_database(url)
    metadata.create_all(engine)


@pytest.fixture()
async def client():
    """
    Ensure full database rollbacks between test cases when used as fixture.
    """
    try:
        await db.connect()
        async with httpx.AsyncClient(
            app=app, base_url="https://testserver"
        ) as test_client:
            yield test_client
    finally:
        await db.disconnect()


@pytest.fixture()
def inception():
    return datetime.fromisoformat("0001-01-01 00:00:00+00:00")


@pytest.fixture()
def passphrase_hash():
    return "this is a passphrase hash"


@pytest.fixture()
def mock_utcnow_and_passphrase_hasher(mocker, inception, passphrase_hash):
    mocker.patch("starlauth.utils.get_utcnow", return_value=inception, autospec=True)
    mocker.patch(
        "starlauth.utils.get_passphrase_hash",
        return_value=passphrase_hash,
        autospec=True,
    )
