import pytest


@pytest.fixture()
def mock_get_timestamp(mocker):
    return mocker.patch("starlauth.tokens.get_timestamp", autospec=True)
